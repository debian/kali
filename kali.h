/*

 kali.h by Ed Chi, summer 1994

$Id: kali.h,v 1.3 1996/06/11 05:36:08 slevy Exp $
$Log: kali.h,v $
Revision 1.3  1996/06/11 05:36:08  slevy
Rationalize ChangeScale(), etc. routines.
Pass DRAWER object to DrawCurrent().
Represent colors as int's, not GL-specific Colorindex's.

 * Revision 1.2  1994/12/15  05:19:53  munzner
 * add options to load file, specify window placement on command line.
 *
 * Revision 1.1  1994/08/26  16:29:02  chi
 * Initial revision
 *
 * Revision 1.1  1994/08/15  20:05:38  chi
 * Initial revision
 *

*/

#ifndef KALI_H
#define KALI_H

#include "main.h"

/*--------------------------------------- in kali.c */

extern void scheduleRedraw();
extern void ChangeScale(double amount, SYMMETRY *sym);
extern void ChangeRotation(double amount, SYMMETRY *sym);
extern void ChangeRatio(double amount, SYMMETRY* sym);
extern void ChangeAngle(double amount, SYMMETRY *sym);
extern LINE *GrabLine(short i, LINE* first);
extern LINE *DropLine(LINE *L);
extern void SetUpOperatorInterface();
extern void GetCursor(POINT *p, RECTANGLE *r, float zoom);
extern void DrawCurrent(RECTANGLE *rect, LINE *Lines, SYMMETRY *sym,
		 POINT *pts, MATRIX *xforms, int count,
		 DRAWER *drawer, int GridDisplay);
extern void HighLiteLine(RECTANGLE *rect, LINE *cur, SYMMETRY *sym,
		  POINT *pts, MATRIX *xforms, int count,
		  int colour);
extern void UseRightCopy(LINE *cur, SYMMETRY *sym, MATRIX *xforms, short
		  pos);
extern WINDOW MakeWindow(RECTANGLE *r, RECTANGLE *kwin);
extern void AdjustWindowRectangle(RECTANGLE *r);
extern int closest(POINT *new_pt, POINT *pts, int maxpts);
extern void PrintLine(LINE* l);
extern void DumpLines(LINE* Lines);
extern void complain(RECTANGLE *win_rect, float zoom);
extern LINE* ThrowAwayLines(LINE *Lines);
extern void SaveProc(FILE *pat);
extern void LoadProc(FILE *pat);


/*----------------------------------------- variable decl in kali.c */

extern int frieze;
extern int moron;
extern int GridDisplay;


extern float zoom;
extern RECTANGLE win_rect,sym_rect;
extern int mode;
extern POINT *sym_pts;
extern int count;
extern MATRIX xforms[5];
extern SYMMETRY *sym;
extern SYMMETRY sym_storage;
extern int sym_index;
extern LINE *Lines;
extern int pick_for;
extern void (*xformfnc) (double, SYMMETRY *);
extern int lit;
extern WINDOW win;


#endif











