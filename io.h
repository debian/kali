/*
  io.h
  by Ed H. Chi (chi@geom.umn.edu)  summer 1994

$Id: io.h,v 1.3 1996/10/01 01:29:52 slevy Exp $
$Log: io.h,v $
Revision 1.3  1996/10/01 01:29:52  slevy
Declare new GL-like routines.

Revision 1.2  1996/06/04 17:07:18  slevy
Toss obsolete routines.

 * Revision 1.1  1994/08/26  16:29:02  chi
 * Initial revision
 *
 * Revision 1.1  1994/08/15  20:05:38  chi
 * Initial revision
 *

*/

#ifndef IO_H
#define IO_H

#include "main.h"

extern int iomode;
extern RECTANGLE clip_rect;

void PrintOut(FILE *outfile,SYMMETRY *sym, RECTANGLE win_rect, LINE *Lines);

/*
 * GX (minimal GL-like thing under X) declarations.
 */
void ortho2(float left, float right, float bottom, float top);
void prefposition(int x0, int x1, int y0, int y1);
int  winopen(char *title);
void swapbuffers(void);
void winset(int w);
void winconstraints(void);
void foreground(void);
void getsize(long *x, long *y);
void doublebuffer(void);
void gconfig(void);
void RGBmode(void);
void reshapeviewport(void);
void cpack(int abgr);
void clear(void);

#ifdef _X11_XLIB_H_
void GXinit(DRAWER *drawer, Display *dpy, Window win);
#endif

/************************************************************************
 * PostScript specific stuff
 ***********************************************************************/

void DrawClippingRectangle(RECTANGLE *rec);
void init_postscript(RECTANGLE *rec);
void close_postscript();
LINE *SpewObject(LINE *obj);

#endif
