/* 
  printmain.c   originally by Tamara Munzner and Nina Amenta

  rewritten by Ed H. Chi summer 1994

  $Id: printmain.c,v 1.2 1994/08/26 16:29:02 chi Exp $
  $Log: printmain.c,v $
 * Revision 1.2  1994/08/26  16:29:02  chi
 * rewritten by Ed H. Chi
 * start of port to X11
 *

*/

#include <stdio.h>
#include <stdlib.h>

#include "symmetry.h"
#include "io.h"

/* printmain also need one, just as kali.c needs one */
WINDOW win = 0;
RECTANGLE sym_rect;

int main(int argc, char *argv[])
{
  FILE *pat;
  RECTANGLE win_rect;
  SYMMETRY *sym;
  LINE *Lines;
  float scale;
  int i;
  
  if (argc<2) {
    printf("file name on command line please!\n");
  } else {
    pat = fopen(argv[1],"r");
    if (pat == NULL) {
      fprintf(stderr,"No such input file \n");
      return EXIT_FAILURE;
    }
    fscanf(pat,"%d\n",&i);
    if (i < 0 || i >= NUM_SYM) {
      fprintf(stderr,"Symmetry group %d out of range\n",i);
      return EXIT_FAILURE;
    }
    sym = &SYMTAB[i];
    fscanf(pat,"%f %f\n",&win_rect.width,&win_rect.height);
    fscanf(pat,"%f %f\n",&(sym->v1.x),&(sym->v1.y));
    fscanf(pat,"%f %f\n",&(sym->v2.x),&(sym->v2.y));
    fscanf(pat,"%f\n",&scale);
    Lines = NULL;
    Lines = ReadPattern(Lines,pat);
    
    win_rect.x = win_rect.y = 0.0;
    PrintOut(stdout, sym, win_rect, Lines);
  }

  return EXIT_SUCCESS;
}
