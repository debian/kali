/* Form definition file generated with fdesign. */

#include "forms.h"
#include "panel.h"

FL_FORM *KaliForm;

FL_OBJECT
        *MoveButton,
        *ZoomButton,
        *RotateButton,
        *AngleButton,
        *RatioButton,
        *LoadButton,
        *SaveButton,
        *QuitButton,
        *DrawButton,
        *InfoButton,
        *PrintButton,
        *GridButton,
        *DimChoice,
        *Frieze,
        *HopBitmap,
        *HopButton,
        *StepBitmap,
        *StepButton,
        *JumpBitmap,
        *JumpButton,
        *SpinJumpBitmap,
        *SpinJumpButton,
        *SidleBitmap,
        *SidleButton,
        *SpinSidleBitmap,
        *SpinSidleButton,
        *SpinHopBitmap,
        *SpinHopButton,
        *Wallpaper,
        *P6MBitmap,
        *PMMBitmap,
        *CMMBitmap,
        *P3Bitmap,
        *P2Bitmap,
        *PMGBitmap,
        *PGGBitmap,
        *CMBitmap,
        *P4Bitmap,
        *PMBitmap,
        *P6Bitmap,
        *P31MBitmap,
        *P4MBitmap,
        *P4GBitmap,
        *PGBitmap,
        *P1Bitmap,
        *P3M1Bitmap,
        *P6MButton,
        *P6Button,
        *P31MButton,
        *P4MButton,
        *P4GButton,
        *P4Button,
        *PGButton,
        *P3M1Button,
        *PMMButton,
        *CMMButton,
        *P3Button,
        *P2Button,
        *PMGButton,
        *PGGButton,
        *CMButton,
        *PMButton,
        *P1Button,
        *SavePSButton;

void create_form_KaliForm()
{
  FL_OBJECT *obj;
  KaliForm = fl_bgn_form(FL_NO_BOX,220.0,730.0);
  obj = fl_add_box(FL_UP_BOX,0.0,0.0,220.0,730.0,"");
  MoveButton = obj = fl_add_button(FL_PUSH_BUTTON,70.0,648.0,60.0,30.0,"Move");
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
    fl_set_call_back(obj,PickProc,KALIMOVE);
  ZoomButton = obj = fl_add_button(FL_PUSH_BUTTON,10.0,618.0,60.0,30.0,"Zoom");
    fl_set_call_back(obj,LineProc,KALIZOOM);
  RotateButton = obj = fl_add_button(FL_PUSH_BUTTON,70.0,618.0,60.0,30.0,"Rotate");
    fl_set_call_back(obj,LineProc,KALIROTATE);
  AngleButton = obj = fl_add_button(FL_PUSH_BUTTON,10.0,588.0,60.0,30.0,"Angle");
    fl_set_call_back(obj,AngleProc,KALIANGLE);
  RatioButton = obj = fl_add_button(FL_PUSH_BUTTON,70.0,588.0,60.0,30.0,"Ratio");
    fl_set_call_back(obj,RatioProc,KALIRATIO);
  LoadButton = obj = fl_add_button(FL_NORMAL_BUTTON,140.0,619.0,66.0,29.5,"Load");
    fl_set_call_back(obj,StartLoadProc,0);
  SaveButton = obj = fl_add_button(FL_NORMAL_BUTTON,140.0,589.5,66.0,29.5,"Save");
    fl_set_call_back(obj,StartSaveProc,0);
  obj = fl_add_text(FL_NORMAL_TEXT,5.0,690.0,210.0,30.0,"KALI");
    fl_set_object_lcol(obj,4);
    fl_set_object_lsize(obj,FL_LARGE_FONT);
    fl_set_object_align(obj,FL_ALIGN_CENTER);
    fl_set_object_lstyle(obj,FL_ENGRAVED_STYLE);
  QuitButton = obj = fl_add_button(FL_NORMAL_BUTTON,155.0,690.0,45.0,25.0,"Quit");
    fl_set_call_back(obj,QuitProc,0);
  DrawButton = obj = fl_add_button(FL_PUSH_BUTTON,10.0,648.0,60.0,30.0,"Draw");
    fl_set_object_lcol(obj,4);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
    fl_set_call_back(obj,DrawProc,KALIDRAW);
  InfoButton = obj = fl_add_button(FL_NORMAL_BUTTON,10.0,690.0,45.0,25.0,"Info");
    fl_set_call_back(obj,InfoProc,0);
  PrintButton = obj = fl_add_button(FL_NORMAL_BUTTON,140.0,648.5,66.0,29.5,"Print");
    fl_set_object_lcol(obj,4);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
    fl_set_call_back(obj,PrintProc,0);
  GridButton = obj = fl_add_button(FL_PUSH_BUTTON,40.0,558.0,60.0,30.0,"Grid");
    fl_set_call_back(obj,GridProc,0);
  DimChoice = obj = fl_add_choice(FL_NORMAL_CHOICE,10.0,525.0,200.0,30.0,"");
    fl_set_call_back(obj,DimProc,0);
  Frieze = fl_bgn_group();
  HopBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,30.0,445.0,100.0,50.0,"hop");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  HopButton = obj = fl_add_button(FL_HIDDEN_BUTTON,30.0,445.0,100.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,HOP);
  StepBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,30.0,205.0,100.0,50.0,"step");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  StepButton = obj = fl_add_button(FL_HIDDEN_BUTTON,30.0,205.0,100.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,STEP);
  JumpBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,30.0,325.0,100.0,50.0,"jump");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  JumpButton = obj = fl_add_button(FL_HIDDEN_BUTTON,30.0,325.0,100.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,JUMP);
  SpinJumpBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,30.0,145.0,100.0,50.0,"spinjump");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  SpinJumpButton = obj = fl_add_button(FL_HIDDEN_BUTTON,30.0,145.0,100.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,SPINJUMP);
  SidleBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,30.0,265.0,100.0,50.0,"sidle");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  SidleButton = obj = fl_add_button(FL_HIDDEN_BUTTON,30.0,265.0,100.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,SIDLE);
  SpinSidleBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,30.0,85.0,100.0,50.0,"spinsidle");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  SpinSidleButton = obj = fl_add_button(FL_HIDDEN_BUTTON,30.0,85.0,100.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,SPINSIDLE);
  SpinHopBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,30.0,385.0,100.0,50.0,"spinhop");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  SpinHopButton = obj = fl_add_button(FL_HIDDEN_BUTTON,30.0,385.0,100.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,SPINHOP);
  obj = fl_add_box(FL_NO_BOX,10.0,70.0,200.0,450.0,"");
  fl_end_group();
  Wallpaper = fl_bgn_group();
  P6MBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,470.0,50.0,50.0,"*632\np6m");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  PMMBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,470.0,50.0,50.0,"*2222\npmm");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  CMMBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,420.0,50.0,50.0,"2*22\ncmm");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P3Bitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,270.0,50.0,50.0,"333\np3");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P2Bitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,370.0,50.0,50.0,"2222\np2");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  PMGBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,320.0,50.0,50.0,"22*\npmg");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  PGGBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,270.0,50.0,50.0,"22x\npgg");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  CMBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,220.0,50.0,50.0,"x*\ncm");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P4Bitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,120.0,50.0,50.0,"442\np4");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  PMBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,120.0,50.0,50.0,"**\npm");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P6Bitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,420.0,50.0,50.0,"632\np6");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P31MBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,370.0,50.0,50.0,"3*3\np31m");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P4MBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,220.0,50.0,50.0,"*442\np4m");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P4GBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,170.0,50.0,50.0,"4*2\np4g");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  PGBitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,105.0,170.0,50.0,50.0,"xx\npg");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_RIGHT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P1Bitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,80.0,70.0,50.0,50.0,"o\np1");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P3M1Bitmap = obj = fl_add_bitmap(FL_NORMAL_BITMAP,55.0,320.0,50.0,50.0,"*333\np3m1");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
    fl_set_object_color(obj,29,7);
    fl_set_object_align(obj,FL_ALIGN_LEFT);
    fl_set_object_lstyle(obj,FL_BOLD_STYLE);
  P6MButton = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,470.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P6M);
  P6Button = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,420.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P6);
  P31MButton = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,370.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P31M);
  P4MButton = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,220.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P4M);
  P4GButton = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,170.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P4G);
  P4Button = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,120.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P4);
  PGButton = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,170.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,PG);
  P3M1Button = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,320.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P3M1);
  PMMButton = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,470.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,PMM);
  CMMButton = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,420.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,CMM);
  P3Button = obj = fl_add_button(FL_HIDDEN_BUTTON,55.0,270.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P3);
  P2Button = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,370.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P2);
  PMGButton = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,320.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,PMG);
  PGGButton = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,270.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,PGG);
  CMButton = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,220.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,CM);
  PMButton = obj = fl_add_button(FL_HIDDEN_BUTTON,105.0,120.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,PM);
  P1Button = obj = fl_add_button(FL_HIDDEN_BUTTON,80.0,70.0,50.0,50.0,"");
    fl_set_object_boxtype(obj,FL_NO_BOX);
    fl_set_call_back(obj,SymmetryProc,P1);
  obj = fl_add_box(FL_NO_BOX,10.0,70.0,200.0,450.0,"");
  fl_end_group();
  obj = fl_add_text(FL_NORMAL_TEXT,4.0,42.0,206.0,26.0,"Left button: drag to draw/move");
  obj = fl_add_text(FL_NORMAL_TEXT,4.0,26.0,206.0,22.0,"Middle button: delete selected");
  obj = fl_add_text(FL_NORMAL_TEXT,4.0,8.0,206.0,22.0,"Right button: click to select");
  SavePSButton = obj = fl_add_button(FL_NORMAL_BUTTON,140.0,560.0,66.0,29.5,"Print To");
    fl_set_call_back(obj,SavePSProc,0);
  fl_end_form();
}

/*---------------------------------------*/

FL_FORM *InfoForm;

FL_OBJECT
        *InfoBrowser,
        *DoneButton;

void create_form_InfoForm()
{
  FL_OBJECT *obj;
  InfoForm = fl_bgn_form(FL_NO_BOX,500.0,390.0);
  obj = fl_add_box(FL_UP_BOX,0.0,0.0,500.0,390.0,"");
  InfoBrowser = obj = fl_add_browser(FL_NORMAL_BROWSER,0.0,30.0,500.0,360.0,"");
    fl_set_object_boxtype(obj,FL_BORDER_BOX);
  DoneButton = obj = fl_add_button(FL_NORMAL_BUTTON,420.0,0.0,80.0,30.0,"Done");
    fl_set_call_back(obj,CloseThisPanel,0);
  fl_end_form();
}

/*---------------------------------------*/

void create_the_forms()
{
  create_form_KaliForm();
  create_form_InfoForm();
}

