#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <stdio.h>

#include "main.h"

void forms_init(RECTANGLE *pwin);
void load_a_file(FILE *pat);
void set_mode(int newmode);

#endif
