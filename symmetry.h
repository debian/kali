/*

 symmetry.h - Nina Amenta, Aug. 1989
 rewritten by Ed Chi summer 1994

$Id: symmetry.h,v 1.9 1996/06/04 17:11:17 slevy Exp $
$Log: symmetry.h,v $
Revision 1.9  1996/06/04 17:11:17  slevy
Pass lots of routines a DRAWER object structure rather than just a
single drawing-routine pointer.

 * Revision 1.8  1994/08/26  16:29:02  chi
 * new symmetry.h file, old content are moved into main.h
 * this is now a header file for symmetry.c file
 *
 * Revision 1.2  1994/08/15  20:05:38  chi
 * significant code cleanup.
 * header files created.
 * main.h created
 * changed #defines in main.h from DRAW to KALIDRAW, CUT to .... etc
 *

*/

#ifndef SYMMETRY_H
#define SYMMETRY_H

#include "main.h"

/*---------------------------------------------- symmetry.c stuff */

extern SYMMETRY SYMTAB[];

extern void DefineSymWindow(RECTANGLE *s_rect, SYMMETRY *sym,
		     RECTANGLE *w_rect, float zoom);
extern int SetUpSymmetry(SYMMETRY *s, POINT **pointpointer, MATRIX xforms[],
		  RECTANGLE *s_rec, RECTANGLE *W_rec);
extern void RearrangeVectors(SYMMETRY *s, VECTOR *vec1,VECTOR *vec2);
extern int PickTwo(VECTOR *in1, VECTOR *in2,
	    VECTOR *vec1, VECTOR *vec2);
extern void SetUpParameterization(VECTOR *vec1, VECTOR *vec2,
			  MATRIX std2scr, MATRIX scr2std);
extern void SetUpRefl(VECTOR *v, MATRIX xform, MATRIX std2scr);
extern void SetUpRot(double angle, float *xform);
extern LINE *MakeCurrentObject(LINE *Lines, SYMMETRY *sym, MATRIX xforms[],
			RECTANGLE *bounds);
extern void FreeObject(LINE *obj);
extern LINE *AllRotations(LINE *l, MATRIX xform, SYMMETRY *sym, LINE*obj,
		   RECTANGLE *bounds, int *pos_count);
extern LINE *AddLineToObject(LINE *obj, LINE *l, RECTANGLE *r,int *pos_count);
extern void AdjustBoundingBox(LINE *l, RECTANGLE *r);
extern void GetMinMax(float a, float b, float c, float d,
	       float *min, float *max);
extern float GetMax(float a, float b);
extern void ReplicateObject(RECTANGLE *rect, LINE *obj, POINT *pts,
		     int count, RECTANGLE *bounds, SYMMETRY *s,
		     DRAWER *drawer);
extern void ClosestEndpoint(LINE *line, POINT *p);
extern float distance(float qx, float qy, float px, float py);
extern void ReplicateOffEdges(RECTANGLE *screen, LINE *obj, POINT *p,
		       RECTANGLE *bounds, SYMMETRY *s,
		       DRAWER *drawer);
extern void DrawIfNecessary(RECTANGLE *screen, LINE *obj, POINT *p,
		     RECTANGLE *bounds, VECTOR *vec, int direction,
		     DRAWER *drawer, int depth);
extern int BBoxIntersectsRect(POINT *p, RECTANGLE *bounds, RECTANGLE
		       *screen);
extern void TranslateLine(LINE *s, VECTOR *v, int scale, LINE *d);
extern void CopyMatrix(MATRIX f, MATRIX t);
extern void CopyLine(LINE *f, LINE *t);
extern void CopyVector(VECTOR *v, VECTOR *w);
extern void CopyRectangle(RECTANGLE *f, RECTANGLE *t);
extern void AddVector(VECTOR *v, VECTOR *w, int scale, VECTOR *d);
extern void MatrixMultiply(MATRIX s, MATRIX t, MATRIX d);
extern void VectorMatrixMult(VECTOR *s, MATRIX m, VECTOR *d);
extern void VectorScalarMult(VECTOR *v, float f);
extern void InvertMatrix(MATRIX s, MATRIX d);
extern void NewId(LINE *line);
extern LINE *NewLine(LINE *Lines);
extern LINE *ReadPattern(LINE *Lines, FILE *pat);
extern LINE *ReadLine(FILE *pat);

#endif
